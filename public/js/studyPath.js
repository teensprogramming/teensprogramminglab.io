var nodes = null;
var edges = null;
var network = null;

var LENGTH_MAIN = 80,
  FONT_SIZE = 30,
  WIDTH_SCALE = 1,
  GREEN = 'green',
  RED = '#C5000B',
  ORANGE = 'orange',
  //GRAY = '#666666',
  GRAY = 'gray',
  BLACK = '#2B1B17';

// Called when the page is loaded.
function draw() {
  nodes = [];
  edges = [];

  color = 'red';

  nodes.push({
    id: 1,
    label: 'Web101',
    group: 'practical',
    font: {
      size: FONT_SIZE
    },
    value: 80
  });
  nodes.push({
    id: 2,
    label: 'CSS framework',
    font: {
      size: FONT_SIZE
    },
    group: 'practical',
    value: 8
  });
  edges.push({
    from: 1,
    to: 2,
    length: LENGTH_MAIN,
    width: WIDTH_SCALE * 6,
    arrows: 'to',
    color: color,
    fontColor: color,
    label: ''
  });
  nodes.push({
    id: 4,
    label: 'Server Programming',
    font: {
      size: FONT_SIZE
    },
    group: 'practical',
    value: 8
  });
  nodes.push({
    id: 444,
    label: 'ES6 and JS framework',
    font: {
      size: FONT_SIZE
    },
    group: 'practical',
    value: 8
  });

  nodes.push({
    id: 44,
    label: 'Database',
    font: {
      size: FONT_SIZE
    },
    group: 'practical',
    value: 8
  });

  edges.push({
    from: 2,
    to: 44,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  edges.push({
    from: 8,
    to: 444,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  edges.push({
    from: 2,
    to: 4,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  nodes.push({
    id: 5,
    label: 'Animation',
    font: {
      size: FONT_SIZE
    },
    group: 'creative',
    value: 8
  });
  edges.push({
    from: 1,
    to: 5,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });

  // javascript
  edges.push({
    from: 1,
    to: 10,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  nodes.push({
    id: 8,
    label: 'JS Dom',
    font: {
      size: FONT_SIZE
    },
    group: 'practical',
    value: 8
  });
  edges.push({
    from: 10,
    to: 8,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  nodes.push({
    id: 9,
    label: 'Python Data Analysis',
    font: {
      size: FONT_SIZE
    },
    group: 'creative',
    value: 8
  });
  edges.push({
    from: 15,
    to: 9,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });

  nodes.push({
    id: 99,
    label: 'R Data Analysis',
    font: {
      size: FONT_SIZE
    },
    group: 'creative',
    value: 8
  });
  edges.push({
    from: 15,
    to: 99,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  // game
  nodes.push({
    id: 10,
    label: 'Web game(Core JS)',
    font: {
      size: FONT_SIZE
    },
    group: 'creative',
    value: 8
  });
  nodes.push({
    id: 111,
    label: 'Java GUI',
    font: {
      size: FONT_SIZE
    },
    group: 'practical',
    value: 8
  });
  edges.push({
    from: 10,
    to: 111,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  // competition

  nodes.push({
    id: 14,
    label: 'CCC101',
    font: {
      size: FONT_SIZE
    },
    group: 'competition',
    value: 80
  });

  nodes.push({
    id: 15,
    label: 'CCC201',
    font: {
      size: FONT_SIZE
    },
    group: 'competition',
    value: 8
  });
  edges.push({
    from: 14,
    to: 15,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });

  nodes.push({
    id: 24,
    label: 'CCC302-C++',
    font: {
      size: FONT_SIZE
    },
    group: 'competition',
    value: 8
  });
  nodes.push({
    id: 25,
    label: 'CCC303-C++',
    font: {
      size: FONT_SIZE
    },
    group: 'competition',
    value: 8
  });

  nodes.push({
    id: 23,
    label: 'CCC201-C++',
    font: {
      size: FONT_SIZE
    },
    group: 'competition',
    value: 8
  });
  nodes.push({
    id: 16,
    label: 'CCC301',
    font: {
      size: FONT_SIZE
    },
    group: 'competition',
    value: 8
  });
  nodes.push({
    id: 26,
    label: 'CCC302',
    font: {
      size: FONT_SIZE
    },
    group: 'competition',
    value: 8
  });
  nodes.push({
    id: 27,
    label: 'CCC303',
    font: {
      size: FONT_SIZE
    },
    group: 'competition',
    value: 8
  });
  edges.push({
    from: 15,
    to: 16,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  edges.push({
    from: 15,
    to: 16,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  edges.push({
    from: 15,
    to: 23,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  edges.push({
    from: 23,
    to: 24,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });
  edges.push({
    from: 24,
    to: 25,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });

  nodes.push({
    id: 17,
    label: 'AP computer science',
    font: {
      size: FONT_SIZE
    },
    group: 'academic',
    value: 8
  });
  edges.push({
    from: 16,
    to: 10,
    arrows: 'to',
    //    dashes: true,
    width: WIDTH_SCALE * 6,
    label: ''
  });
  edges.push({
    from: 16,
    to: 17,
    arrows: 'to',
    //    dashes: true,
    width: WIDTH_SCALE * 6,
    label: ''
  });
  edges.push({
    from: 16,
    to: 26,
    arrows: 'to',
    //    dashes: true,
    width: WIDTH_SCALE * 6,
    label: ''
  });
  edges.push({
    from: 26,
    to: 27,
    arrows: 'to',
    dashes: true,
    width: WIDTH_SCALE * 6,
    label: ''
  });
  nodes.push({
    id: 18,
    label: 'CCC302-Java',
    font: {
      size: FONT_SIZE
    },
    group: 'competition',
    value: 8
  });
  edges.push({
    from: 17,
    to: 18,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });


  nodes.push({
    id: 20,
    label: 'Java certificate',
    font: {
      size: FONT_SIZE
    },
    group: 'practical',
    value: 8
  });
  edges.push({
    from: 17,
    to: 20,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });

  nodes.push({
    id: 21,
    label: 'C',
    font: {
      size: FONT_SIZE
    },
    group: 'academic',
    value: 8
  });

  edges.push({
    from: 16,
    to: 21,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });

  edges.push({
    from: 21,
    to: 22,
    arrows: 'to',
    width: WIDTH_SCALE * 6,
    label: ''
  });

  // pygame
  //  nodes.push({
  //    id: 23,
  //    label: 'Pygame',
  //    font: {
  //      size: FONT_SIZE
  //    },
  //    group: 'creative',
  //    value: 8
  //  });

  //  edges.push({
  //    from: 15,
  //    to: 23,
  //    arrows: 'to',
  //    width: WIDTH_SCALE * 6,
  //    label: ''
  //  });

  // legend
  var study_path = document.getElementById('study_path');
  //    var x = -study_path.clientWidth / 2 + 50;
  //    var y = -study_path.clientHeight / 2 + 50;
  var x = -study_path.clientWidth / 2 - 200;
  var y = -study_path.clientHeight / 2 - 200;
  var step = 70;
  nodes.push({
    id: 1000,
    x: x,
    y: y,
    label: 'Creative',
    group: 'creative',
    value: 1,
    fixed: true,
    physics: false
  });
  nodes.push({
    id: 1001,
    x: x,
    y: y + step,
    label: 'Practical',
    group: 'practical',
    value: 1,
    fixed: true,
    physics: false
  });
  nodes.push({
    id: 1002,
    x: x,
    y: y + 2 * step,
    label: 'Competition',
    group: 'competition',
    value: 1,
    fixed: true,
    physics: false
  });
  nodes.push({
    id: 1003,
    x: x,
    y: y + 3 * step,
    label: 'Academic',
    group: 'academic',
    value: 1,
    fixed: true,
    physics: false
  });

  // create a network
  var container = document.getElementById('study_path');
  var data = {
    nodes: nodes,
    edges: edges
  };
  var options = {
    nodes: {
      scaling: {
        min: 16,
        max: 32
      }
    },
    edges: {
      color: GRAY,
      smooth: false
    },
    physics: {
      barnesHut: {
        gravitationalConstant: -30000
      },
      stabilization: {
        iterations: 2500
      }
    },
    interaction: {
      zoomView: false,
      dragView: false
    },
    groups: {
      'practical': {
        shape: 'square',
        color: '#FF9900' // orange
      },
      academic: {
        shape: 'square',
        color: "#2B7CE9" // blue
      },
      competition: {
        shape: 'square',
        color: "#C5000B" // red
      },
      creative: {
        shape: 'square',
        color: "#109618" // green
      }
    }
  };
  network = new vis.Network(container, data, options);
}

draw();
