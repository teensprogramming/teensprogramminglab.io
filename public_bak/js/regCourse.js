$(function() {
    var $name = $('#name');
    var $email = $('#email');
    var $agegroup = $('#agegroup');
    var $guardianname = $('#guardianname');
    var $guardianemail = $('#guardianemail');
    var $guardianphone = $('#guardianphone');

    var $regbutton = $('#regbutton');
    var $regCourseMsg= $('#regCourseMsg');

    $regbutton.click(function() {
        var name = $name.val();
        var email = $email.val();
        var agegroup = $agegroup.val();
        var guardianname = $guardianname.val();
        var guardianemail = $guardianemail.val();
        var guardianphone = $guardianphone.val();

        var data = JSON.stringify({
            "stuFullName": name,
            "stuEmail": email,
            "stuAgeGroup": agegroup,
            "guarFullName": guardianname,
            "guarEmail": guardianemail,
            "guarPhone": guardianphone});
        var url = "/regcourse";

        console.log(data);
        console.log(url);
        $.ajax({
            url : url,
            type: "POST",
            contentType : "application/json; charset=utf-8",
            datatype: "json",
            data: data,
            success: function(res) {
                $regCourseMsg.text(res.regCourseMsg);
                tp.jswpRegStatus();
            },
            error: function(xhr, textStatus, errorThrown) {

            }
        });
    });
});
