$(function() {
    var $submitbtn = $('#subscribebtn');
    var $email = $('#subscribeemail');
    var $subscribemsg = $('#subscribemsg');

    $submitbtn.click(function() {
        var email = $email.val();
        var data = JSON.stringify({"subscribeemail":email});
        var url = "/subscribe";
        $.ajax({
            url : url,
            type: "POST",
            contentType : "application/json; charset=utf-8",
            datatype: "json",
            data: data,
            success: function(res) {
                $subscribemsg.text(res.subscribemsg);
            },
            error: function(xhr, textStatus, errorThrown) {

            }
        });
    });
});
