data = [
    {
        '2020 Canadian Computing Competition Senior': [
            {
                'name': 'Abhishek',
                'certificate_distinction': 1
           },
            {
                'name': 'Daniel',
                'certificate_distinction': 1
           },
            {
                'name': 'Jerry',
                'certificate_distinction': 1
           },
            {
                'name': 'Jesse',
                'certificate_distinction': 1
           },
            {
                'name': 'Lucas',
                'certificate_distinction': 1
           }
       ],
        '2020 Canadian Computing Competition Junior': [
            {
                'name': 'Eric',
                'certificate_distinction': 1
            },
            {
                'name': 'Jake<br>(Full Marks)',
                'certificate_distinction': 1,
                'honour_roll': 1
            },
            {
                'name': 'Jeffery',
                'certificate_distinction': 1
            },
            {
                'name': 'Raluca',
                'certificate_distinction': 1
            },
            {
                'name': 'Richard',
                'certificate_distinction': 1,
                'honour_roll': 1
            },
            {
                'name': 'Kate',
                'certificate_distinction': 1
            },
            {
                'name': 'Sophia',
                'certificate_distinction': 1
            },
            {
                'name': 'Luke',
                'certificate_distinction': 1,
                'honour_roll': 1
            },
            {
                'name': 'Lawrence',
                'certificate_distinction': 1
            }

        ]

    },
    {
        '2019 Canadian Computing Competition Senior': [
            {
                'name': 'Daniel',
                'certificate_distinction': 1
      },
            {
                'name': 'Carol',
                'certificate_distinction': 1
      }
    ],
        '2019 Canadian Computing Competition Junior': [
            {
                'name': 'Allon',
                'certificate_distinction': 1,
                'honour_roll': 1
      },
            {
                'name': 'Andrew',
                'certificate_distinction': 1
      },
            {
                'name': 'Jennifer',
                'certificate_distinction': 1
      },
            {
                'name': 'Tina',
                'certificate_distinction': 1
      },
            {
                'name': 'Abhishek',
                'certificate_distinction': 1
      },
            {
                'name': 'Jerry Zhao',
                'certificate_distinction': 1
      },
            {
                'name': 'Jesse',
                'certificate_distinction': 1
      },
            {
                'name': 'Lucas',
                'certificate_distinction': 1
      },
            {
                'name': 'Raluca',
                'certificate_distinction': 1
      },
            {
                'name': 'Yuchen',
                'certificate_distinction': 1
      },
    ]
  },
    {
        '2018 Canadian Computing Competition Junior': [
            {
                'name': 'Alan',
                'certificate_distinction': 1
      },
            {
                'name': 'Andrew',
                'certificate_distinction': 1
      },
            {
                'name': 'Carol',
                'certificate_distinction': 1
      },
            {
                'name': 'Daniel',
                'certificate_distinction': 1
      },
            {
                'name': 'Dennis',
                'certificate_distinction': 1,
      },
            {
                'name': 'David',
                'certificate_distinction': 1
      },
            {
                'name': 'Jennifer',
                'certificate_distinction': 1
      },
            {
                'name': 'Jerry Lin',
                'certificate_distinction': 1,
      },
            {
                'name': 'Raluca',
                'certificate_distinction': 1
      },
            {
                'name': 'Shinjan',
                'certificate_distinction': 1
      },
            {
                'name': 'TianQi',
                'certificate_distinction': 1
      },
            {
                'name': 'Tom',
                'certificate_distinction': 1,
      }
    ]
  },
    {
        '2017 Canadian Computing Competition Junior': [
            {
                'name': 'David',
                'certificate_distinction': 1
      },
            {
                'name': 'Jerry Lin',
                'certificate_distinction': 1,
      },
            {
                'name': 'Tom',
                'certificate_distinction': 1,
                'honour_roll': 1
      },
    ]
  },
];

var awards = {
    'certificate_distinction': 'Certificate of Distinction',
    'honour_roll': 'Student Honour Roll',
}

function makeTable(contest, results, columns) {
    var table = d3.select('#competition_results .results .table-responsive').append('table')
        .attr('class', 'table table-hover')

    // append the caption
    table.append('caption')
        .text(contest);

    var tbody = table.append('tbody');

    // create a row for each object in the data
    var rows = tbody.selectAll('tr')
        .data(results)
        .enter()
        .append('tr');

    // create a cell in each row for each column
    var cells = rows.selectAll('td')
        .data(function (row) {
            return columns.map(function (column) {
                if (column === 'name') {
                    return {
                        column: column,
                        text: row[column]
                    }
                } else {
                    if (row[column] === 1) {
                        var displayText = awards[column];
                    } else {
                        var displayText = '';
                    }
                    return {
                        column: column,
                        text: displayText
                    };
                }
            });
        })
        .enter()
        .append('td')
        .append('span')
        .attr('class', function (d) {
            if (d.column === 'name') {
                return 'name';
            }
        })
        .text(function (d) {
            return d.text;
        });

    return table;
}

function tabulate_competition_results(data, columns) {

    for (var i = 0; i < data.length; i++) {
        var award = data[i];
        for (var key in award) {
            var contest = key;
            var results = award[key];
            makeTable(contest, results, columns);
        }
        d3.select('#competition_results').append('br');
    }
}

// render the table
tabulate_competition_results(data, ['name', 'certificate_distinction', 'honour_roll']);
