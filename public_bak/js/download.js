// var env = require('jsdom').env;
// var jswindow = require('jsdom').jsdom().parentWindow;
// var $ = require('jquery')(jswindow);
// var fs = require('fs');

// var xhr = new XMLHttpRequest();
// var url = 'download/HTML5_Elements_Pic.pdf';
// xhr.open('GET', url, true);
// xhr.responseType = 'arraybuffer';

// xhr.onload = function(e) {
  // response is unsigned 8 bit integer
  // var responseArray = new Uint8Array(this.response);
  // console.log('receive size: '+responseArray.byteLength);
  // window.open(url, '_blank');
// };
// xhr.send();

/* JQuery ajax receive data size less than original pdf file */
/* for example, the pdf file on server side is 75696, while JQuery get 73104 */
/* JQuery can't handle binary file download without implement JQuery.ajaxTransport() */

// $(function() {
//     var url = "http://localhost:3000/download/HTML5_Elements_Pic.pdf";
//     $.ajax({
//       processData: false,
//       url : url,
//       contentType: "application/pdf",

//       success: function(data) {
//         console.log('success downloading file');
//         console.log('download size: '+data.length);
//         window.open(url, "_blank");
//       },

//       error: function(xhr, textStatus, errorThrown) {
//         console.log("error", errorThrown);
//         console.log(xhr);
//       }
//     });
//   });
