$(function() {
    function jswpRegStatus () {
        var $jswpRegStatus = $('#jswpRegStatus');
        var url = "/regcourse/status";
        $.ajax({
            url : url,
            type: "GET",
            contentType : "application/json; charset=utf-8",
            datatype: "json",
            success: function(res) {
                var msg;
                if (res.isRegEnd) {
                    msg = 'Registration is over, please subscribe our newsletter.';
                } else {
                    if (res.remaining < 0) {
                        msg = '('+Math.abs(res.remaining)+' Students in waiting list)';
                    }else if (res.remaining === 0) {
                        msg ='(No seats available, but we have a waiting list)';
                    }else {
                        msg = '(' + res.remaining + ' Seats Remaining)';
                    }
                }
                $jswpRegStatus.text(msg);
            },
            error: function(xhr, textStatus, errorThrown) {
            }
        });
    }

    jswpRegStatus();

    tp.jswpRegStatus = jswpRegStatus;
});
