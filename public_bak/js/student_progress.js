/*
        'name':'',
        "wd101":1,
        'wd102':1,
        'animation':1,
        'ccc101':1,
        'ccc201':1,
        'ccc301':1

    {
		'name':'Iris',
		"wd101":1,
		'wd102':1,
    },
    {
		'name':'David Liu',
		"wd101":1,
		'wd102':1,
    },

    {
        'name':'Cindy',
        "wd101":1,
        'ccc101':1,
    },

    {
        'name':'Ben',
        'wd101':1,
        'ccc101':1,
        'ccc102':1
    },

    {
        'name':'Steven Hu',
        "wd101":1,
        'ccc101':1,

        'ccc201':1
    },{
    'name':'Alan',
    "wd101":1, 
    'ccc101':1,
    'ccc201':1,
    'wd201':1,
    'game1':1
},

*/

data = [    {
        'name':'Abhishek',
        'ccc101':1
    },
    {
        'name':'Andrew',
        "ccc101":1, 
        'ccc201':1
    },
    {
        'name':'Annica',
        "wd101":1,
        'wd102':1,
        'animation':1,
        'ccc101':1,
        'ccc201':1,
        'wd201':1,
        'game1':1
    },
    {
        'name':'Ballerina',
        'wd101':1,
        'ccc101':1
    },
    {
        'name':'Brandon',
        'wd101':1
    },
    {
        'name':'Carol',
        'wd101':1,
        "ccc101":1,
        'ccc201':1,
        'ccc301':1,
        'apcs':1
    },
    {
        'name':'Charles',
        'wd101':1,
        'wd102':1,
        'animation':1,
        'ccc101':1
    },
    {
        'name':'David Lin',
        'wd101':1,
        'wd102':1,
        'ccc101':1,
        'ccc201':1,
        'ccc301':1,
        'wd201':1,
        'game1':1,
        'apcs':1
    },
    {
        'name':'Dennis',
        'wd101':1,
        'ccc101':1,
        'ccc201':1,
        'ccc301':1,
        'wd201':1,
        'game1':1,
        'apcs':1
    },
    {
        'name':'Eric',
        "wd101":1,
        'wd102':1,
        'animation':1,
        'ccc101':1
    },
    {
        'name':'Ethan',
        "wd101":1,
        'animation':1,
        'ccc101':1
    },
    {
        'name':'Hadil',
        'wd101':1
    },
    {
        'name':'Henry',
        "wd101":1,
        'ccc101':1,
        'ccc201':1,
        'wd201':1,
        'game1':1,
        'apcs':1
    },
    {
        'name':'Jake',
        'wd101':1,
        'animation':1,
        'ccc101':1
    },
    {
        'name':'Jason',
        'wd101':1,
        'ccc101':1
    },
    {
        'name':'Jennifer',
        "wd101":1,
        'wd102':1,
        'animation':1,
        'ccc101':1,
        'ccc201':1,
        'wd201':1,
        'game1':1
    },
    {
        'name':'Jerry Lin',
        "wd101":1,
        'wd102':1,
        'ccc101':1,
        'ccc201':1,
        'ccc301':1,
        'wd201':1,
        'game1':1,
        'apcs':1
    },
    {
        'name':'Jerry Zhao',
        "wd101":1,
        'ccc101':1,
        'wd201':1,
        'game1':1,
        'apcs':1
    },
    {
        'name':'Jesse',
        'wd101':1,
        'ccc101':1
    },
    {
        'name':'Jia Qi',
        "wd101":1,
        'ccc101':1
    },
    {
        'name':'Joan',
        "wd101":1,
        'animation':1,
        'ccc101':1
    },
    {
        'name':'Lucas',
        'ccc101':1,
        'apcs':1
    },
    {
        'name':'Madhu',
        'wd101':1,
        'ccc101':1
    },
    {
        'name':'Marina',
        "wd101":1,
        'animation':1
    },
    {
		'name':'Mckenna',
        'ccc101':1
    },
    {
        'name':'Raluca',
        'ccc101':1
    },
    {
        'name':'Rohan',
        'wd101':1
    },
    {
        'name':'Shinjan',
        'wd101':1,
        'ccc101':1
    },
    {
        'name':'Sophia',
        "wd101":1
    },
    {
        'name':'Steve',
        "wd101":1
    },
    {
        'name':'Tian Qi',
        'wd101':1,
        'ccc101':1
    },
    {
        'name':'Tina',
        'wd101':1,
        'ccc101':1,
        'ccc201':1
    },
    {
        'name':'Tom',
        "wd101":1,
        'ccc101':1,
        'ccc201':1,
        'ccc301':1,
        'wd201':1,
        'game1':1,
        'apcs':1
    },
];

var courses={
    'wd101':'Web page',
    'wd102':'Web page project',
    'animation':'Animation',
    'ccc101':'CCC101',
    'ccc201':'CCC201',
    'ccc301':'CCC301',
    'ccc302':'CCC302',
    'ccc303':'CCC303',
    'apcs':'AP CS',
    'wd201':'Core JS',
    'game1':'Game1',
    'game2':'Game Projects',
    'game3':'Mobile Game',
    'mobile1':'Mobile Apps',
    'server':'Server',
    'viz':'Viz data'
}

function tabulate(data, columns) {
    var table = d3.select('#progress .table-responsive').append('table')
        .attr('id','student_progress_table').attr('class','table table-hover table-bordered')
    var thead = table.append('thead')
    var tbody = table.append('tbody');

    // append the header row
    //			thead.append('tr')
    //				.selectAll('th')
    //				.data(columns).enter()
    //				.append('th')
    //				.text(function(column) {
    //					return column;
    //				});

    // create a row for each object in the data
    var rows = tbody.selectAll('tr')
        .data(data)
        .enter()
        .append('tr');

    // create a cell in each row for each column
    var cells = rows.selectAll('td')
        .data(function(row) {
            return columns.map(function(column) {
                if (column==='name') {
                    return {
                        column:column,
                        text:row[column]
                    }
                } else {
                    return {
                        column:column,
                        text: courses[column],
                        finish:row[column]
                    };
                }
            });
        })
        .enter()
        .append('td')
        .attr('class',function(d) {
            if (d.column!=='name') {
                if (d.finish)
                    return 'y' 
                else
                    return 'n'
            }
        })
        .text(function(d) {
            return d.text;
        });

    return table;
}

// render the table
tabulate(data, ['name','wd101', 'wd102','animation','ccc101','ccc201','ccc301','ccc302','ccc303','apcs','wd201','game1','game2','game3','mobile1','server','viz']); 

// navigation table by keyup and keydowno
var student_progress_table = document.getElementById('student_progress_table');
var start_row=student_progress_table.getElementsByTagName('tr')[0];
start_row.focus();
start_row.style.backgroundColor = 'red';

function dotheneedful(next_row) {
  if (next_row != null) {
    start_row.focus();
    start_row.style.backgroundColor = '';
    start_row.style.color = '';
    next_row.focus();
    next_row.style.backgroundColor = 'red';
    start_row = next_row;
  }
}

document.onkeydown = function (e) {
  e = e || window.event;
  if (e.keyCode === 38) {
    // up arrow
    var idx = start_row.rowIndex;
    var nextrow = start_row.parentElement.rows[idx-1];
    dotheneedful(nextrow);
  } else if (e.keyCode === 40) {
    // down arrow
    var idx = start_row.rowIndex;
    var nextrow = start_row.parentElement.rows[idx+1];
    dotheneedful(nextrow);
  } 
}

