$(function() {
    // create map
    // var latitude = 43.02093;
    var latitude = 43.020933;
    // var longitude = -81.25472;
    var longitude = -81.2569107;
    var mapOptions = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 14,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    // create marker
    var markerOptions = {
        position: new google.maps.LatLng(latitude, longitude)
    };
    var marker = new google.maps.Marker(markerOptions);
    marker.setMap(map);
    // create info window
    var infoWindowOptions = {
        content: 'Teens Programming at 655 Windermere Rd'
    };

    var infoWindow = new google.maps.InfoWindow(infoWindowOptions);
    // google.maps.event.addListener(marker,'click',function(e){
    //   infoWindow.open(map, marker);
    // });
    infoWindow.open(map, marker);
});
