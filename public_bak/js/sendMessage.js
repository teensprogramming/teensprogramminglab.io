$(function() {
    var $submitbtn = $('#sendMsgBtn');
    var $name = $('#msgName');
    var $email=$('#msgEmail');
    var $msg = $('#msg');
    var $response = $('#sendMsgResponse');

    $submitbtn.click(function() {
        var name = $name.val();
        var email = $email.val();
        var msg = $msg.val();
        var data = JSON.stringify({
            "name":name,
            "email":email,
            "msg":msg});
        var url = "/sendmessage";
        $.ajax({
            url : url,
            type: "POST",
            contentType : "application/json; charset=utf-8",
            datatype: "json",
            data: data,
            success: function(res) {
                $response.text(res.sendMsgResponse);
                $response.removeClass('error');
                $response.addClass('success');
            },
            error: function(xhr, textStatus, errorThrown) {
                $response.text(xhr.responseText);
                $response.addClass('error');
            }
        });
    });
});
